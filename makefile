# define the commands we will use for compilation and library building
AR = ar
ARFLAGS = rcs
CC = gcc
CXX = g++
VPATH = .

# define useful flags to cc/ld/etc.
CFLAGS += -std=c11 -g -Wall -Wpedantic -I. -I../jansson/include -O0
#CFLAGS += -std=c11 -O3 -Wall -Wpedantic -I. -I../jansson/include -O0
CPPFLAGS += -std=c++11 -g -Wall -Wpedantic -I. -I../jansson/include -I./test -O0
LDFLAGS += -L. -L../jansson/lib -larray2d -ljansson
TESTFLAGS += -L. -L../jansson/lib -L../gtest -lhw2 -ljansson -l:gtest_main.a

#replace these definitions with lists of your source and header files
TESTSRC=hw2.c
LIBOBJS=array2D.o
HEADERS=array2D.h array2D_priv.h

UNITTESTSRC=test/*.cc

all: hw2 libarray2d.a

tests: $(LIBOBJS) $(UNITTESTSRC)
	$(CXX) $(CPPFLAGS) $(LIBOBJS) $(UNITTESTSRC) -o $@ \
	$(TESTFLAGS) -lpthread $(LDFLAGS)

libarray2d.a: $(LIBOBJS) $(HEADERS)
	$(AR) $(ARFLAGS) libarray2d.a $(LIBOBJS)

hw2:  libarray2d.a $(TESTSRC) $(HEADERS)
	$(CC) $(CFLAGS) $(TESTSRC) -o hw2 $(LDFLAGS)

run: hw2
	valgrind --leak-check=full ./hw2 test.json

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $<

clean:
	/bin/rm -f *.o *~ hw2 libarray2d.a json.out tests
