/*
 * Ryan Matthew Smith, rsmith24
 * David Grant, dagr
 * CSE333 WI17
 */

#include <stdlib.h>  //malloc
#include <stdbool.h> //basic boolean
#include <string.h>  //memcopy

#include <jansson.h>

#include "array2D_priv.h"
#include "array2D.h"

Array2D Array2D_new(int rows, int cols, size_t dataSize) {
  //interior bounds check
  if (rows < 1 || cols < 1){
    return NULL;
  }

  Array2D new2D = (Array2D)malloc(sizeof(Array2DStruct));

  //mem check
  if (new2D == NULL){
    return NULL;
  }
  new2D->data = malloc(rows*cols*dataSize);

  //mem check
  if (new2D->data == NULL){
    // Earlier malloc succeeded, so we must free that.
    free(new2D);
    return NULL;
  }

  new2D->dataSize = dataSize;
  new2D->row = rows;
  new2D->col = cols;

  return new2D;
}

Array2D Array2D_copy(Array2D arr) {
  if (arr == NULL) {
    return NULL;
  }

  Array2D newCopy = Array2D_new(arr->row, arr->col, arr->dataSize);

  if (newCopy == NULL) {
    return NULL;
  }

  // Copy data payload contents.

  memcpy(newCopy->data, arr->data,
   newCopy->row * newCopy->col * newCopy->dataSize);

  return newCopy;
}

Array2D Array2D_newFromJson(json_t* jsonRoot, size_t dataSize,
        DeserializeCallback itemCallback) {
  if (jsonRoot == NULL || dataSize <= 0 || itemCallback == NULL) {
    return NULL;
  }

  // Example payload:
  // {"rows": 2, "columns":3, "data":[0,1,2,3,4,5]}

  // These are borrowed and we don't need to incref/decref:
  json_t* rowsJson = json_object_get(jsonRoot, "rows");
  json_t* columnsJson = json_object_get(jsonRoot, "columns");
  json_t* dataJson = json_object_get(jsonRoot, "data");

  if (!json_is_integer(rowsJson) || !json_is_integer(columnsJson) ||
      !json_is_array(dataJson)) {
    return NULL;
  }

  int rows = json_integer_value(rowsJson);
  int columns = json_integer_value(columnsJson);

  Array2D Array2D = Array2D_new(rows, columns, dataSize);

  size_t index;
  json_t* arrayValue;

  json_array_foreach(dataJson, index, arrayValue) {
    // Give the callback the json_t* and the address where it should
    // store the value. (This gives the client great responsibility.)
    itemCallback(arrayValue, Array2D->data + index * Array2D->dataSize);
  }

  return Array2D;
}

json_t* Array2D_arrayToJson(Array2D arr, SerializeCallback itemCall){
  if(arr == NULL)return NULL;
  json_t* jsonRoot = json_object(); //new empty json

  //get row col, add to Json root
  json_t* rows = json_integer(arr->row);
  json_t* cols = json_integer(arr->col);

  json_object_set_new(jsonRoot, "rows", rows);
  json_object_set_new(jsonRoot, "columns", cols);

  //set empty array json key/value pair
  json_t* json_Array = json_array();

  //serialize arr
  json_t* json_unkValue;
  ArrValue value;

  for(int i = 0; i < arr->row; i++) {
    for(int j = 0; j < arr->col; j++) {
      Array2D_get(arr, i, j, &value);
      itemCall(&json_unkValue, value);
      json_array_append_new(json_Array, json_unkValue);
    }
  }
  //place filled array
  json_object_set_new(jsonRoot, "data", json_Array);

  return jsonRoot;
}

void Array2D_free(Array2D arr) {
  if (arr == NULL) {
    return;
  }

  //free the data first
  free(arr->data);

  free(arr);
}

void Array2D_freeEx(Array2D arr, FreeItemCallback freeElements){
  if(arr == NULL) return;

  ArrValue value;

  if (freeElements!= NULL){
    for (int i=0;i<arr->row;i++){
      for (int j=0;j<arr->col;j++){
        Array2D_get(arr, i, j, &value);
        freeElements(value);
      }
    }
  }
  Array2D_free(arr);
}

bool Array2D_swap(Array2D arr, int row1, int col1, int row2, int col2) {
  if (arr == NULL)return false;
  if (!Array2D_bound(arr, row1, col1) || !Array2D_bound(arr,row2,col2)) {
    return false;
  }
  //hold first val
  ArrValue temp = malloc(sizeof(arr->dataSize));
  memcpy(temp, Array2D_pointerArrValue(arr, row1, col1), arr->dataSize);

  //swap
  memcpy(Array2D_pointerArrValue(arr, row1, col1),
      Array2D_pointerArrValue(arr, row2, col2), arr->dataSize);
  memcpy(Array2D_pointerArrValue(arr, row2, col2), temp, arr->dataSize);

  free(temp);

  return true;
}

bool Array2D_get(Array2D arr, int row, int col, ArrValue* result) {
  if (arr == NULL || result == NULL || !Array2D_bound(arr, row, col)) {
    return false;
  }

  *result = Array2D_pointerArrValue(arr, row, col);
  return true;
}

bool Array2D_set(Array2D arr, int row, int col, ArrValue val) {
  if (arr == NULL)return false;
  if (!Array2D_bound(arr, row, col)) return false;

  memcpy(Array2D_pointerArrValue(arr, row, col), val, arr->dataSize);
  return true;
}


bool Array2D_bound(Array2D arr, int row, int col) {
  if (arr == NULL) {
    return false;
  }

  return row >= 0 && row < arr->row
   && col >= 0 && col < arr->col;
}

ArrValue Array2D_pointerArrValue(Array2D arr, int row, int col) {
  //full rows*columns + column of current row, all times dataSize
  int shift = (arr->col*row + col) * arr->dataSize;

  ArrValue elementPoint = arr->data + shift;
  return elementPoint;
}

int Array2D_getRows(Array2D arr) {
  if (arr == NULL || arr->row < 1) return -1;
  return arr->row;
}

int Array2D_getCols(Array2D arr) {
  if (arr == NULL || arr->col < 0) return -1;
  return arr->col;
}

size_t Array2D_getDataSize(Array2D arr) {
  if (arr == NULL || arr->dataSize < 1) return -1;
  return arr->dataSize;
}

Iter_Array2D Array2D_iterNew(Array2D arr){
  if (arr == NULL)return NULL;
    
  Iter_Array2D iter = (Iter_Array2D)malloc(sizeof(Array2DIter));
  if (iter == NULL) return NULL; //out of memory

  iter->pos=0;
  iter->elements=(arr->row*arr->col);
  iter->arrIter=arr;

  return iter;
}

bool Array2D_iterHasNext(Iter_Array2D iter){
  
  if(iter == NULL)return false;
  
  if(iter->pos >= iter->elements) return false;

  return true; 
}

ArrValue Array2D_iterNext(Iter_Array2D iter){
  if(iter == NULL ||
     iter->pos >= iter->elements) return NULL;
  ArrValue ret = iter->arrIter->data+
                 (iter->arrIter->dataSize*(iter->pos));
  iter->pos++;
  return ret;
}

void Array2D_iterFree(Iter_Array2D iter){
  free(iter);
}
