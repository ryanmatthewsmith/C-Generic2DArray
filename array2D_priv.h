#ifndef ARRAY2D_PRIV_H
#define ARRAY2D_PRIV_H

#include <stdlib.h>

#include "array2D.h"

typedef struct arr2D_st{
    int        row;      //rows in Array2D
    int        col;      //cols in Array2D
    size_t     dataSize; //dataSize per element in Array
    char*      data;     //use char* for 1 byte minimum ArrValue
} Array2DStruct;

typedef struct Array2D_iter{
  Array2D arrIter;        //array2D* pointer to iterate over
  int     pos;            //current position of the iterator
  int     elements;       //total elements in the iterator
} Array2DIter;
/*
 * pointerArrValue, for internal use.  Returns pointer to start
 * of intended ArrValue in Array2D.
 *
 * Parameters
 *   arr: Array2D
 *   row: index of row in Array2D
 *   col: index of col in Array2D
 *
 * returns: pointer of intended ArrValue
 */
ArrValue Array2D_pointerArrValue(Array2D arr, int row, int col);

#endif //ARRAY2D_PRIV_H
