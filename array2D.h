// Ryan Matthew Smith, rsmith24
// David Grant, dagr
// CSE333 WI 17

#ifndef ARRAY2D_H
#define ARRAY2D_H

#include <stdlib.h>
#include <stdbool.h>

#include <jansson.h>

/*
 * The values of our Array2D are generic, it is the client's job to
 * cast the appropriate type when using values from an Array2D. To
 * supply values client must send a pointer to the intended value
 * cast as an ArrValue or void*.  The client must know the size of
 * their value data type in bytes for newArray. This is a design
 * trade-off to eliminate wasted space for small data types but still
 * allow array of larger data types like any large struct.
 */
typedef void* ArrValue;

/*
 * Item deserialization callback. A deserialization callback implementation
 * should take the given json object and convert it by whatever means
 * necessary, storing the result at the location described by arrayValue.
 */
typedef void (*DeserializeCallback)(json_t* json, ArrValue arrayValue);

/*
 * Serialization callback function type. The serialization callback accepts the
 * location of where the resulting json_t* should be stored, and an ArrValue.
 * Callback implementations should convert arrayValue to a new json_t* and
 * store it at *json. The implementation will take care of deallocating the
 * json_t*.
 */
typedef void (*SerializeCallback)(json_t** json, ArrValue arrayValue);

/*
 * A callback definition for allowing the client to free each item in the data
 * array when calling Array2D_freeEx.
 */
typedef void (*FreeItemCallback)(ArrValue);

/*
 * Client gets a pointer to work with that represents our Array2D
 */
struct arr2D_st;
typedef struct arr2D_st *Array2D;

/*
 *  Client gets pointer to iterator struct, implementation hidden
 */
struct Array2D_iter;
typedef struct Array2D_iter *Iter_Array2D;

/*
 * Allocate a new Array2D.
 * An Array2D must later be deallocated by calling Array2D_free
 * (or Array2D_freeEx).
 *
 * Parameters:
 *  int rows      : number of rows in Array2D
 *  int cols      : number of columns in Array2D
 *  size_t dataSize  : size in bytes of the data type being stored
 *
 * Returns:
 *  NULL on error, Array2D on success
 *
 *  error on out of memory
 *
 */
Array2D Array2D_new(int rows, int cols, size_t dataSize);

/*
 * Allocates and initializes an Array2D from the given JSON fragment.
 * Returned array must be freed by calling Array2D_free or Array2D_freeEx.
 *
 * Parameters:
 *   json: A json_t* object. Must have format:
 *    {"rows": 2, "columns": 3, "data": [elem1, elem2, elem3, elem4]}
 *    where the elem1..4 are arbitrary objects to be
 *    interpreted by itemCallback.
 *
 *   dataSize: Size in bytes of the individual items to be stored.
 *
 *   itemCallback: a function that takes an elemN (passed as a json_t*),
 *    converts it to the desired type, and stores the result at the given
 *    address.
 *
 * returns NULL on error, or an Array2D on success.
 */
Array2D Array2D_newFromJson(json_t* json, size_t dataSize,
 DeserializeCallback itemCallback);

/*
 * Copy an Array2D.
 * Must be later freed with Array2D_free.
 */
Array2D Array2D_copy(Array2D);

/*
 * Produces and returns JSON fragment from a passed Array2D
 * Parameters:
 *   Array2D arr: Array2D to convert to JSON
 *   SerializeCallback itemCall:  client is responsible for passing a function
 *   which will serialize a given element to json object and append the object
 *   into the json_array passed to the function
 * Returns:
 *   json_t* representing entire Array2D arr
 *   NULL on error
 * returned json object will be of form:
 * {"rows": 2, "columns": 3, "data": [elem1, elem2, elem3, elem4, ...]}
 * Caller is responsible for calling decref on returned json_t*.
 */
json_t* Array2D_arrayToJson(Array2D arr, SerializeCallback itemCall);

/*
 * Free Array2D - frees the passed Array2D.  Client should pass Array2Don
 * that hasn't been freed.  Assigning a Array2D to a new variable does
 * not create a new Array2D.  To create a new Array2D from an existing
 * copy, use copyNew_Array2D method.
 *
 * Parameters:
 *  Array2D arr :Array2D to be freed
 *
 */
void Array2D_free(Array2D arr);

/*
 * Frees the Array2D, allowing the calling code to specify a
 * function to perform custom deallocation for each item in the array.
 * This is necessary whenever client
 * is storing pointers in Array2D.  All stored elements will be
 * freed by function call on each element, then the Array2D
 * will be freed.
 *
 * Parameters:
 *   Array2D arr: Array2D to be freed
 *   FreeItemCallBack freeElements : client provided function to free
 *   individual elements.
 */
void Array2D_freeEx(Array2D arr, FreeItemCallback freeElements);

/*
 * Swaps 2 ArrValue locations in Array2D.
 *
 * Parameters:
 *   arr  : Array2D to swap ArrValue
 *   row1 : int of row index from first Array2D
 *   col1 : int of col index from first Array2D
 *   row2 : int of row index from second Array2D
 *   col2 : int of col index from first Array2D
 * Returns:
 *   true:  when successful
 *   false: when not
 */
bool Array2D_swap(Array2D arr, int row1, int col1, int row2, int col2);

/*
 * Fetches the ArrValue at given row and col index.  It's up to
 * client to know if the value has previously been set.  Array2D_get
 * will check bounds and return null for outOfBounds calls.
 *
 * Parameters:
 *   arr  : Array2D to set ArrValue
 *   row  : int of row index to get
 *   col  : int of col index to get
 *   value: location where the ArrValue should be stored..
 * Returns:
 *   true:  on success, false otherwise.
 */
bool Array2D_get(Array2D arr, int row, int col, ArrValue* value);

/*
 * Sets ArrValue in Array2D at provided index.
 *
 * Parameters:
 *   arr  : Array2D to set ArrValue
 *   row  : int of row index to set
 *   col  : int of col index to set
 *   val  : ArrValue to set at row, col
 * Returns:
 *   true:  when successful
 *   false: when not
 */
bool Array2D_set(Array2D arr, int row, int col, ArrValue val);

/*
 * Checks bounds for supplied row and col.
 *
 * Parameters:
 *   arr  : Array2D we are checking
 *   row  : row for bounds check
 *   col  : col for bounds check
 *
 * Returns:
 *    Whether or not the specified row and column are valid indices.
 */
bool Array2D_bound(Array2D arr, int row, int col);

/*
 * Returns the number of rows in the given Array2D.
 *
 * Parameters:
 *   arr: Array2D
 * Returns:
 *   int, rows in Array2D
 *   returns -1 if arr is NULL
 */
int Array2D_getRows(Array2D arr);

/*
 * Returns the number of columns in the passed Array2D.
 *
 * Parameters:
 *   arr: Array2D
 * Returns:
 *   int, columns in Array2D
 *   returns -1 if arr is NULL
 */
int Array2D_getCols(Array2D arr);

/*
 * Returns the size of individual elements stored in the Array2D.
 * (This is just the dataSize given at creation time.)
 *
 * Parameters:
 *   arr: Array2D
 * Returns:
 *   size_t, size of each ArrValue in Array2D
 *   returns -1 if arr is NULL
 */
size_t Array2D_getDataSize(Array2D arr);

/*
 * Creates an Iterator for an Array2D.
 * Returned iterator must later be freed by calling
 * Array2D_iterFree.
 *
 * Parameters:
 *   Array2D arr: the Array2D to make an iterator of
 * Returns:
 *   Iter_Array2D: returns the iterator
 *   returns NULL is passed arr is NULL
 *   returns NULL when out of memory
 */
Iter_Array2D Array2D_iterNew(Array2D arr);

/*
 * Checks it iterator has a next element
 *
 * Parameters:
 *   Iter_Array2D iter: Array2D iterator
 * Returns:
 *   true: when next element exists
 *   false: when no next element or NULL iter
 */
bool Array2D_iterHasNext(Iter_Array2D iter);

/*
 * Returns next ArrValue in iterator
 *
 * Parameters:
 *   Iter_Array2D iter: Array2D iterator
 * Returns:
 *   ArrValue: when next element exists
 *   NULL: otherwise
 */
ArrValue Array2D_iterNext(Iter_Array2D iter);

/*
 * Frees the provided Array2D iterator
 *
 * Parameters:
 *   Iter_Array2D: Array2D iterator
 */
void Array2D_iterFree(Iter_Array2D iter);

#endif // ARRAY2D_H
